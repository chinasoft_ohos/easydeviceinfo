# easydeviceinfo

#### 项目介绍
- 项目名称：easydeviceinfo
- 所属系列：openharmony 第三方组件适配移植
- 功能：方便的获取手机设备的各种数据信息的库
- 项目移植状态：完成部分功能模块（缺少EasyCpuMod，Total Internal Memory，WiFi SSID,BT MAC Address,Supported ABIS,Manufacturer，Running on emulator，User-Agent,Build Brand,Pseudo ID
,openharmony ID,Display Version等）
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 2.4.1

#### 效果演示
<div align="left">
  <img src="img/easy.gif" />
</div>

#### 安装教程

在moudle级别下的build.gradle文件中添加依赖，在dependencies标签中增加对libs目录下jar包的引用  
 ```
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}

// 添加依赖库
dependencies {
    
    // 基础库
    implementation 'com.gitee.chinasoft_ohos:easydeviceinfo_base:1.0.1'

}
 ```


在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
EasyDeviceInfo被分割成多个模块库，从v2.2.0开始。根据你的要求使用合适的一个。
+  `easydeviceinfo`
    - 主库，引用了这两个 `easydeviceinfo-ads` and `easydeviceinfo-base`.
+ `easydeviceinfo-ads`
    - EasyDevicelnfo 广告库，它有助于提供关于广告的信息。
    -  **支持库**
    + [`EasyAdsMod`](easydeviceinfo_ads/src/main/java/github/nisrulz/easydeviceinfo/ads/EasyAdsMod.java) 未完成.

+ `easydeviceinfo-base`
    -  EasyDeviceInfo Base, 关于设备的信息.
    -  **支持库**
        + [EasyAppMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyAppMod.java)
        + [EasyBatteryMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyBatteryMod.java)
        + [EasyBluetoothMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyBluetoothMod.java)
        + [EasyConfigMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyConfigMod.java)
        + [EasyDeviceMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyDeviceMod.java)
        + [EasyDisplayMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyDisplayMod.java)
        + [EasyIdMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyIdMod.java)
        + [EasyLocationMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyLocationMod.java)
        + [EasyMemoryMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyMemoryMod.java)
        + [EasyNetworkMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyNetworkMod.java)
        + [EasyNfcMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyNfcMod.java)
        + [EasySimMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasySimMod.java)
        + [EasySensorMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasySensorMod.java)
        + [EasyFingerprintMod](easydeviceinfo_base/src/main/java/github/nisrulz/easydeviceinfo/base/EasyFingerprintMod.java)

简单例子

创建一个mods 实例 ( **Easy\*Mod** class ),例如 EasyConfigMod
```java
EasyConfigMod easyConfigMod = new EasyConfigMod(context);
```
下一步在 ***easyConfigMod*** 实例上调用可用的方法
```java
String time_in_ms= String.valueOf(easyConfigMod.getTime());
```
每个 **Mods** 都有一组特定的方法，你可以调用它们来获取设备信息. 例如：  **EasyConfigMod**

|Value|functionName|returns
|---|---|---|
|Time (ms)|`getTime()`|long
|Formatted Time (24Hr)|`getFormattedTime()`|String
|Up Time (ms)|`getUpTime()`|long
|Formatted Up Time (24Hr)|`getFormattedUpTime()`|String

#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与原组件基本无差异  

#### 版本迭代
- 1.0.1

    
