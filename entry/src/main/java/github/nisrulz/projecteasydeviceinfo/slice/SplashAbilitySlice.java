/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.projecteasydeviceinfo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.security.SystemPermission;

import github.nisrulz.projecteasydeviceinfo.ResourceTable;
import github.nisrulz.projecteasydeviceinfo.RuntimePermissionUtil;

/**
 * @since 2021-04-26
 */
public class SplashAbilitySlice extends AbilitySlice {
    private static final String[] requestBasicPermissions =
        {
            SystemPermission.LOCATION,
            SystemPermission.LOCATION_IN_BACKGROUND
        };

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_splash);
        final Button btnReqPermission = (Button) findComponentById(ResourceTable.Id_btn_req);
        btnReqPermission.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                RuntimePermissionUtil.requestPermission(getAbility(), requestBasicPermissions, 100);
            }
        });
    }
}
