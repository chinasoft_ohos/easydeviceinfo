package github.nisrulz.projecteasydeviceinfo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

import github.nisrulz.projecteasydeviceinfo.slice.MainAbilitySlice;

public class MainAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(Color.getIntColor("#303F9F"));
        setMainRoute(MainAbilitySlice.class.getName());
    }
}
