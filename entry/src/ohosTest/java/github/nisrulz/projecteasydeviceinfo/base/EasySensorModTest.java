package github.nisrulz.projecteasydeviceinfo.base;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasySensorMod;

public class EasySensorModTest {

    private EasySensorMod easySensorMod;

    @Before
    public void setUp() throws Exception {
        easySensorMod = new EasySensorMod();
    }

    @Test
    public void getAllSensors() {
        Assert.assertNotNull(easySensorMod.getAllSensors());
    }
}