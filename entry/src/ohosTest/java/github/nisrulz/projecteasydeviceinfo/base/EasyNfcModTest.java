package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyNfcMod;

public class EasyNfcModTest {
    private EasyNfcMod easyNfcMod;

    @Before
    public void setUp() throws Exception {
        easyNfcMod = new EasyNfcMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void isNfcPresent() {
        Assert.assertFalse(easyNfcMod.isNfcPresent());
    }

    @Test
    public void isNfcEnabled() {
        Assert.assertFalse(easyNfcMod.isNfcEnabled());
    }
}