package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyNetworkMod;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class EasyNetworkModTest {
    private EasyNetworkMod easyNetworkMod;

    @Before
    public void setUp() throws Exception {
        easyNetworkMod = new EasyNetworkMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void isWifiEnabled() {
        assertTrue(easyNetworkMod.isWifiEnabled());
    }

    @Test
    public void getIPv4Address() {
     assertNotNull(easyNetworkMod.getIPv4Address());
    }

    @Test
    public void getIPv6Address() {
        assertNotNull(easyNetworkMod.getIPv6Address());
    }

    @Test
    public void getWifiSSID() {
        assertEquals("Chinasoft",easyNetworkMod.getWifiSSID());
    }

    @Test
    public void getWifiLinkSpeed() {
        assertEquals("54 Mbps",easyNetworkMod.getWifiLinkSpeed());
    }
}