package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasySimMod;

public class EasySimModTest {

    private EasySimMod easySimMod;

    @Before
    public void setUp() throws Exception {
        easySimMod = new EasySimMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void getCountry() {
        Assert.assertEquals("cn",easySimMod.getCountry());
    }

    @Test
    public void isSimNetworkLocked() {
        Assert.assertFalse(easySimMod.isSimNetworkLocked());
    }

    @Test
    public void getCarrier() {
        Assert.assertNotNull(easySimMod.getCarrier());
    }

    @Test
    public void isMultiSim() {
        Assert.assertFalse(easySimMod.isMultiSim());
    }

    @Test
    public void getNumberOfActiveSim() {
        Assert.assertEquals(1,easySimMod.getNumberOfActiveSim());
    }
}