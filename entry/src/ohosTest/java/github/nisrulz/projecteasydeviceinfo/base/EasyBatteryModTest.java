package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.BatteryHealth;
import github.nisrulz.easydeviceinfo.base.ChargingVia;
import github.nisrulz.easydeviceinfo.base.EasyBatteryMod;
import github.nisrulz.projecteasydeviceinfo.ResourceTable;

public class EasyBatteryModTest {
    private EasyBatteryMod easyBatteryMod;

    @Before
    public void setUp() throws Exception {
        easyBatteryMod = new EasyBatteryMod(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
    }

    @Test
    public void getBatteryPercentage() {
        int bPercentage = easyBatteryMod.getBatteryPercentage();
        Assert.assertEquals(100,bPercentage);
    }

    @Test
    public void isDeviceCharging() {
        boolean deviceCharging = easyBatteryMod.isDeviceCharging();
        Assert.assertTrue(deviceCharging);
    }

    @Test
    public void getBatteryHealth() {
        int batteryHealth = easyBatteryMod.getBatteryHealth();
        Assert.assertEquals(1,batteryHealth);
    }

    @Test
    public void getBatteryTechnology() {
        String batteryTechnology = easyBatteryMod.getBatteryTechnology();
        Assert.assertEquals("Li-ion",batteryTechnology);
    }

    @Test
    public void getBatteryTemperature() {
        float batteryTemperature = easyBatteryMod.getBatteryTemperature();
        Assert.assertEquals(28.0,batteryTemperature,0.1);
    }

    @Test
    public void getBatteryVoltage() {
        int batteryVoltage = easyBatteryMod.getBatteryVoltage();
        Assert.assertEquals(4371,batteryVoltage);
    }

    @Test
    public void getChargingSource() {
        int chargingSource = easyBatteryMod.getChargingSource();
        Assert.assertEquals(0,chargingSource);

    }

    @Test
    public void isBatteryPresent() {
       easyBatteryMod.isBatteryPresent(new EasyBatteryMod.BatteryCallback() {
            @Override
            public void onSuccess(boolean b) {
                Assert.assertTrue(b);
            }
        });

    }
}