package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.DeviceType;
import github.nisrulz.easydeviceinfo.base.EasyDeviceMod;
import github.nisrulz.projecteasydeviceinfo.ResourceTable;

public class EasyDeviceModTest {
    private EasyDeviceMod easyDeviceMod;

    @Before
    public void setUp() throws Exception {
        easyDeviceMod = new EasyDeviceMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void getDeviceType() {
        int deviceType = easyDeviceMod.getDeviceType(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
        Assert.assertEquals(DeviceType.PHONE,deviceType);
    }

    @Test
    public void getBuildVersionSDK() {
        int buildVerSdk=easyDeviceMod.getBuildVersionSDK();
        Assert.assertEquals(5,buildVerSdk);
    }

    @Test
    public void getOSCodename() {
        String osCodeName=easyDeviceMod.getOSCodename();
        Assert.assertEquals("Release",osCodeName);
    }
}