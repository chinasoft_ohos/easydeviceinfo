package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyConfigMod;

public class EasyConfigModTest {
    private EasyConfigMod easyConfigMod;

    @Before
    public void setUp() throws Exception {
        easyConfigMod = new EasyConfigMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void hasSdCard() {
        boolean hasSdCard = easyConfigMod.hasSdCard();
        Assert.assertTrue(hasSdCard);
    }

    @Test
    public void getTime() {
        long time =easyConfigMod.getTime();
        Assert.assertNotNull(time);
    }

    @Test
    public void getFormattedTime() {
        String forMattedTime =easyConfigMod.getFormattedTime();
        Assert.assertNotNull(forMattedTime);
    }

    @Test
    public void getUpTime() {
        long upTime =easyConfigMod.getUpTime();
        Assert.assertNotNull(upTime);
    }
}