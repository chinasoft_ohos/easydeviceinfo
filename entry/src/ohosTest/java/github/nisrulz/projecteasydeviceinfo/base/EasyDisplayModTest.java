package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyDisplayMod;

public class EasyDisplayModTest {
private EasyDisplayMod easyDisplayMod;
    @Before
    public void setUp() throws Exception {
        easyDisplayMod=new EasyDisplayMod(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
    }

    @Test
    public void getDensity() {
        String density=easyDisplayMod.getDensity();
        Assert.assertEquals("XXHDPI",density);
    }

    @Test
    public void getResolution() {
        String resolution=easyDisplayMod.getResolution();
        Assert.assertEquals("2085x1080",resolution);
    }
}