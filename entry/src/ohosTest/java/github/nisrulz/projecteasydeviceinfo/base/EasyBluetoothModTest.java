/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyBluetoothMod;

/**
 * @since 2021-07-07
 */
public class EasyBluetoothModTest {
    @Test
    public void hasBluetoothLeAdvertising() {
        EasyBluetoothMod easyBluetoothMod=new EasyBluetoothMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
        Assert.assertFalse(easyBluetoothMod.hasBluetoothLeAdvertising());
    }
}
