package github.nisrulz.projecteasydeviceinfo.base;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import github.nisrulz.easydeviceinfo.base.EasyAppMod;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EasyAppModTest {
    private EasyAppMod appMod;

    @Before
    public void setUp() throws Exception {
        appMod = new EasyAppMod(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility());
    }

    @Test
    public void getAbilityName() {
        String aName = appMod.getAbilityName();
        Assert.assertEquals("EntryAbility", aName);
    }

    @Test
    public void getPackageName() {
        String pName = appMod.getPackageName();
        assertEquals("github.nisrulz.projecteasydeviceinfo", pName);
    }

    @Test
    public void getAppName() {
        String appName = appMod.getAppName();
        assertEquals("$string:app_name", appName);
    }

    @Test
    public void getAppVersion() {
        String aVer = appMod.getAppVersion();
        assertEquals("1.1", aVer);
    }

    @Test
    public void getAppVersionCode() {
        String aVerCode = appMod.getAppVersionCode();
        assertEquals("1001000", aVerCode);
    }

    @Test
    public void isPermissionGranted() {
        boolean isPer = appMod.isPermissionGranted("ohos.permission.GET_BUNDLE_INFO_PRIVILEGED");
        assertFalse(isPer);
    }
}