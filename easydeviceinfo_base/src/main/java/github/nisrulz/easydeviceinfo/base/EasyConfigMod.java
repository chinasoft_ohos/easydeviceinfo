/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.base;

import ohos.app.Context;
import ohos.data.usage.DataUsage;
import ohos.data.usage.MountState;
import ohos.media.audio.AudioManager;
import ohos.media.audio.AudioRemoteException;
import ohos.miscservices.timeutility.Time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static ohos.media.audio.AudioManager.AudioRingMode.RINGER_MODE_NORMAL;
import static ohos.media.audio.AudioManager.AudioRingMode.RINGER_MODE_SILENT;
import static ohos.media.audio.AudioManager.AudioRingMode.RINGER_MODE_VIBRATE;

/**
 * EasyConfig Mod Class
 *
 * @since 2021-06-10
 */
public class EasyConfigMod {
    private final Context context;
    private AudioManager audioManager;

    /**
     * Instantiates a new Easy config mod.
     *
     * @param context the context
     */
    public EasyConfigMod(final Context context) {
        this.context = context;
    }

    /**
     * Checks if the device has sd card
     *
     * @return the boolean
     */
    public final boolean hasSdCard() {
        return DataUsage.getDiskMountedStatus() == MountState.DISK_MOUNTED && context.getExternalCacheDir() != null;
    }

    /**
     * Gets Device Ringer Mode.
     *
     * @return Device Ringer Mode
     */
    @RingerMode
    public final int getDeviceRingerMode() {
        int ringerMode = RingerMode.NORMAL;
        if (audioManager == null && context != null)
            audioManager = new AudioManager(context);

        if (audioManager != null) {
            try {
                int ringer = audioManager.getRingerMode();
                if (ringer == RINGER_MODE_NORMAL.ordinal()) {
                    ringerMode = RingerMode.NORMAL;
                } else if (ringer == RINGER_MODE_SILENT.ordinal()) {
                    ringerMode = RingerMode.SILENT;
                } else if (ringer == RINGER_MODE_VIBRATE.ordinal()) {
                    ringerMode = RingerMode.VIBRATE;
                }

            } catch (AudioRemoteException e) {
                e.printStackTrace();
            }
        }

        return ringerMode;
    }

    /**
     * Gets time.
     *
     * @return the time
     */
    public final long getTime() {
        return System.currentTimeMillis();
    }

    /**
     * Gets formatted time.
     *
     * @return the formatted time
     */
    public final String getFormattedTime() {
        DateFormat timeInstance = SimpleDateFormat.getTimeInstance();
        return timeInstance.format(Calendar.getInstance().getTime());
    }

    /**
     * Gets up time.
     *
     * @return the up time
     */
    public final long getUpTime() {
        return Time.getRealActiveTime();
    }

    /**
     * Gets formatted up time.
     *
     * @return the formatted up time
     */
    public final String getFormattedUpTime() {
        DateFormat timeInstance = SimpleDateFormat.getTimeInstance();
        return timeInstance.format(Time.getRealActiveTime());
    }

    /**
     * Gets date from milliseconds
     *
     * @return the date
     */
    public final Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * Gets formatted date.
     *
     * @return the formatted date
     */
    public final String getFormattedDate() {
        DateFormat dateInstance = SimpleDateFormat.getDateInstance();
        return dateInstance.format(Calendar.getInstance().getTime());
    }
}

