/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.base;

import ohos.app.Context;
import ohos.bundle.ApplicationInfo;
import ohos.bundle.BundleInfo;
import ohos.bundle.IBundleManager;
import ohos.hiviewdfx.HiLog;
import ohos.rpc.RemoteException;

import github.nisrulz.easydeviceinfo.common.EasyDeviceInfo;

/**
 * EasyApp Mod Class
 *
 * @since 2021-06-10
 */
public class EasyAppMod {
    private static final String NAME_NOT_FOUND_EXCEPTION = "Name Not Found Exception";
    private final Context context;

    /**
     * Instantiates a new Easy app mod.
     *
     * @param context the context
     */
    public EasyAppMod(final Context context) {
        this.context = context;
    }

    /**
     * Gets Ability name.
     *
     * @return the Ability name
     */
    public final String getAbilityName() {
        return CheckValidityUtil.checkValidData(context.getClass().getSimpleName());
    }

    /**
     * Gets package name.
     *
     * @return the package name
     */
    public final String getPackageName() {
        return CheckValidityUtil.checkValidData(context.getBundleName());
    }


    /**
     * Gets app name.
     *
     * @return the app name
     */
    public final String getAppName() {
        String result;
        ApplicationInfo ai = context.getApplicationInfo();
        if (ai == null && EasyDeviceInfo.debuggable) {
            HiLog.debug(EasyDeviceInfo.LABEL, "", NAME_NOT_FOUND_EXCEPTION);
        }
        result = ai != null ? ai.getLabel() : null;
        return CheckValidityUtil.checkValidData(result);
    }

    /**
     * Gets app version.
     *
     * @return the app version
     */
    public final String getAppVersion() {
        String result = null;
        try {
            BundleInfo bundleInfo = context.getBundleManager().getBundleInfo(context.getBundleName(), IBundleManager.GET_BUNDLE_DEFAULT);
            result = bundleInfo.getVersionName();
            return CheckValidityUtil.checkValidData(result);
        } catch (RemoteException e) {
            if (EasyDeviceInfo.debuggable) {
                HiLog.error(EasyDeviceInfo.LABEL, "", NAME_NOT_FOUND_EXCEPTION, e);
            }
        }
        return CheckValidityUtil.checkValidData(result);
    }

    /**
     * Gets app version code.
     *
     * @return the app version code
     */
    public final String getAppVersionCode() {
        String result = null;
        try {
            BundleInfo bundleInfo = context.getBundleManager().getBundleInfo(context.getBundleName(), IBundleManager.GET_BUNDLE_DEFAULT);
            result = String.valueOf(bundleInfo.getVersionCode());
        } catch (RemoteException e) {
            if (EasyDeviceInfo.debuggable) {
                HiLog.error(EasyDeviceInfo.LABEL, "", NAME_NOT_FOUND_EXCEPTION, e);
            }
        }
        return CheckValidityUtil.checkValidData(result);
    }

    /**
     * Is permission granted boolean.
     *
     * @param permission the permission
     * @return the boolean
     */
    public final boolean isPermissionGranted(final String permission) {
        return context.verifyCallingPermission(permission) == IBundleManager.PERMISSION_GRANTED;
    }

    /**
     * Check if the app with the specified packagename is installed or not
     *
     * @param packageName the package name
     * @return the boolean
     */
    public final boolean isAppInstalled(String packageName) {
        try {
            return context.getBundleManager().getLaunchIntentForBundle(packageName) != null;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }
}
