/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.base;

import ohos.app.Context;
import ohos.security.SystemPermission;
import ohos.security.permission.NeedsPermission;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SimInfoManager;
import ohos.telephony.TelephonyConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * EasySim Mod Class
 *
 * @since 2021-06-10
 */
public class EasySimMod {
    private final RadioInfoManager tm;
    private final SimInfoManager sm;
    private final Context context;

    /**
     * Instantiates a new Easy si mod.
     *
     * @param mContext the context
     */
    public EasySimMod(final Context mContext) {
        this.context = mContext;
        tm = RadioInfoManager.getInstance(context);
        sm = SimInfoManager.getInstance(context);
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public final String getCountry() {
        String result;
        if (sm != null && sm.getSimState(sm.getDefaultVoiceSlotId()) == TelephonyConstants.SIM_STATE_READY) {
            result = sm.getIsoCountryCodeForSim(sm.getDefaultVoiceSlotId()).toLowerCase(Locale.getDefault());
        } else {
            Locale locale = Locale.getDefault();
            result = locale.getCountry().toLowerCase(locale);
        }
        return CheckValidityUtil.checkValidData(
            CheckValidityUtil.handleIllegalCharacterInResult(result));
    }

    /**
     * Is sim network locked.
     *
     * @return the boolean
     */
    public final boolean isSimNetworkLocked() {
        return sm != null && sm.getSimState(sm.getDefaultVoiceSlotId()) == TelephonyConstants.SIM_STATE_LOCKED;
    }

    /**
     * Gets carrier.
     *
     * @return the carrier
     */
    public final String getCarrier() {
        String result = tm.getOperatorName(sm.getDefaultVoiceSlotId()).toLowerCase(Locale.getDefault());
        return CheckValidityUtil.checkValidData(
            CheckValidityUtil.handleIllegalCharacterInResult(result));
    }

    /**
     * Gets SIM serial number.
     * <p>
     * You need to declare the below permission in the manifest file to use this properly
     * <p>
     * SystemPermission.GET_TELEPHONY_STATE
     *
     * @return the sim serial
     */
    @NeedsPermission(SystemPermission.GET_TELEPHONY_STATE)
    public final String getSIMSerial() {
        String result = null;
        if (sm != null && PermissionUtil.hasPermission(context, SystemPermission.GET_TELEPHONY_STATE)) {
            result = sm.getSimIccId(sm.getDefaultVoiceSlotId());
        }
        return CheckValidityUtil.checkValidData(result);
    }

    /**
     * Is multi sim.
     * <p>
     * You need to declare the below permission in the manifest file to use this properly
     * <p>
     * SystemPermission.GET_TELEPHONY_STATE
     *
     * @return the boolean
     */
    @NeedsPermission(SystemPermission.GET_TELEPHONY_STATE)
    public final boolean isMultiSim() {
        return getActiveMultiSimInfo().size() > 1;
    }

    /**
     * Gets active multi sim info.
     * <p>
     * You need to declare the below permission in the manifest file to use this properly
     * <p>
     * SystemPermission.GET_TELEPHONY_STATE
     *
     * @return the active multi sim info
     */
    @NeedsPermission(SystemPermission.GET_TELEPHONY_STATE)
    public final List<Integer> getActiveMultiSimInfo() {
        List<Integer> tempActiveSub = new ArrayList<Integer>();
        for (int i = 0; i < sm.getMaxSimCount(); i++) {
            if (sm.isSimActive(i)) {
                tempActiveSub.add(i);
            }
        }
        return tempActiveSub;
    }

    /**
     * Gets number of active sim.
     * <p>
     * You need to declare the below permission in the manifest file to use this properly
     * <p>
     * SystemPermission.GET_TELEPHONY_STATE
     *
     * @return the number of active sim
     */
    @NeedsPermission(SystemPermission.GET_TELEPHONY_STATE)
    public final int getNumberOfActiveSim() {
        return getActiveMultiSimInfo().size();
    }
}
