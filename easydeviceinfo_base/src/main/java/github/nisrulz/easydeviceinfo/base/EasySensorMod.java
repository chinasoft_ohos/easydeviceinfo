/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.base;

import ohos.app.Context;
import ohos.sensor.agent.CategoryBodyAgent;
import ohos.sensor.agent.CategoryEnvironmentAgent;
import ohos.sensor.agent.CategoryLightAgent;
import ohos.sensor.agent.CategoryMotionAgent;
import ohos.sensor.agent.CategoryOrientationAgent;
import ohos.sensor.agent.CategoryOtherAgent;
import ohos.sensor.bean.SensorBase;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Easy sensor mod.
 *
 * @since 2021-04-05
 */
public class EasySensorMod {
    private CategoryOtherAgent categoryOtherAgent;
    private CategoryBodyAgent categoryBodyAgent;
    private CategoryLightAgent categoryLightAgent;
    private CategoryEnvironmentAgent categoryEnvironmentAgent;
    private CategoryMotionAgent categoryMotionAgent;
    private CategoryOrientationAgent categoryOrientationAgent;
    private List list;

    /**
     * Instantiates a new Easy sensor mod.
     */
    public EasySensorMod() {
        initAgent();
    }

    private void initAgent() {
        if (categoryOtherAgent == null) categoryOtherAgent = new CategoryOtherAgent();
        if (categoryBodyAgent == null) categoryBodyAgent = new CategoryBodyAgent();
        if (categoryLightAgent == null) categoryLightAgent = new CategoryLightAgent();
        if (categoryEnvironmentAgent == null) categoryEnvironmentAgent = new CategoryEnvironmentAgent();
        if (categoryMotionAgent == null) categoryMotionAgent = new CategoryMotionAgent();
        if (categoryOrientationAgent == null) categoryOrientationAgent = new CategoryOrientationAgent();
    }

    /**
     * Gets all sensors.
     *
     * @return the all sensors
     */
    public List<SensorBase> getAllSensors() {
        if (list == null) list = new ArrayList<>();
        if (list.size() > 0) {
            return list;
        }
        list.addAll(categoryOtherAgent.getAllSensors());
        list.addAll(categoryBodyAgent.getAllSensors());
        list.addAll(categoryEnvironmentAgent.getAllSensors());
        list.addAll(categoryLightAgent.getAllSensors());
        list.addAll(categoryMotionAgent.getAllSensors());
        list.addAll(categoryOrientationAgent.getAllSensors());
        return list;
    }
}
