/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.base;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Optional;

/**
 * EasyDisplay Mod Class
 *
 * @since 2021-06-10
 */
public class EasyDisplayMod {
    private final Context context;
    private final Optional<Display> display;
    private final DisplayManager dm;

    /**
     * Instantiates a new Easy display mod.
     *
     * @param context the context
     */
    public EasyDisplayMod(final Context context) {
        this.context = context;
        dm = DisplayManager.getInstance();
        if (dm != null) {
            display = dm.getDefaultDisplay(context);
        } else {
            display = null;
        }

    }

    /**
     * Gets density.
     *
     * @return the density
     */
    public final String getDensity() {
        String densityStr = null;
        if (display.isPresent()) {
            final int density = display.get().getRealAttributes().densityDpi;
            switch (density) {
                case DisplayAttributes.LOW_DENSITY:
                    densityStr = "LDPI";
                    break;
                case DisplayAttributes.MEDIUM_DENSITY:
                    densityStr = "MDPI";
                    break;
                case DisplayAttributes.HIGH_DENSITY:
                    densityStr = "HDPI";
                    break;
                case 213:
                    densityStr = "TVDPI";
                    break;
                case 320:
                    densityStr = "XHDPI";
                    break;
                case 400:
                    densityStr = "XMHDPI";
                    break;
                case 480:
                    densityStr = "XXHDPI";
                    break;
                case 640:
                    densityStr = "XXXHDPI";
                    break;
                default:
                    break;
            }
        }
        return CheckValidityUtil.checkValidData(densityStr);
    }

    /**
     * Get display xy coordinates int [ ].
     *
     * @param event the event
     * @return the int [ ]
     */
    public final int[] getDisplayXYCoordinates(final TouchEvent event) {
        int[] coordinates = new int[2];
        coordinates[0] = 0;
        coordinates[1] = 0;
        if (event.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            coordinates[0] = (int) event.getPointerPosition(0).getX();     // X Coordinates
            coordinates[1] = (int) event.getPointerPosition(0).getY();     // Y Coordinates
        }
        return coordinates;
    }

    /**
     * Gets resolution.
     *
     * @return the resolution
     */
    public final String getResolution() {
        if (display.isPresent()) {
            Optional<DisplayAttributes> metrics = Optional.ofNullable(display.get().getAttributes());
            if (metrics.isPresent()) {
                return CheckValidityUtil.checkValidData(metrics.get().height + "x" + metrics.get().width);
            }
        }
        return CheckValidityUtil.checkValidData("");
    }

    public final double getPhysicalSize() {
        if (display.isPresent()) {
            Optional<DisplayAttributes> metrics = Optional.ofNullable(display.get().getAttributes());
            if (metrics.isPresent()) {
                double x = Math.pow(metrics.get().width / metrics.get().xDpi, 2);
                double y = Math.pow(metrics.get().height / metrics.get().yDpi, 2);
                return Math.sqrt(x + y);
            }
        }
        return 0f;
    }
}
