/*
 * Copyright (C) 2016 Nishant Srivastava
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package github.nisrulz.easydeviceinfo.ads;

import ohos.app.Context;

/**
 * The type Easy ads mod.
 *
 * @since 2021-06-10
 */
public class EasyAdsMod {
    private final Context context;

    /**
     * Instantiates a new Easy ads mod.
     *
     * @param context the context
     */
    public EasyAdsMod(Context context) {
        this.context = context;
    }

    /**
     * The interface Ad identifier callback.
     */
    public interface AdIdentifierCallback {
        /**
         * On success.
         *
         * @param adIdentifier the ad identifier
         * @param adDonotTrack the ad donot track
         */
        void onSuccess(String adIdentifier, boolean adDonotTrack);
    }
}
