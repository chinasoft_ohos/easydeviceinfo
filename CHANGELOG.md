## 1.0.1
ohos 第一个 release 版本
 * 实现了原库的大部分api
 * 因为系统不支持，缺少EasyCpuMod，Total Internal Memory，WiFi SSID,BT MAC Address,Supported ABIS,Manufacturer，Running on emulator，User-Agent,Build Brand,Pseudo ID
,openharmony ID,Display Version等数据

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分api
 * 因为系统不支持，缺少EasyCpuMod，Total Internal Memory，WiFi SSID,BT MAC Address,Supported ABIS,Manufacturer，Running on emulator，User-Agent,Build Brand,Pseudo ID
,openharmony ID,Display Version等数据

